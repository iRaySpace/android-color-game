PlayState = State:subclass("PlayState")

function PlayState:initialize()

	local name = "Play"

	State.initialize(self, name)

	player = Player:new()

	gameClock = timer:new()
	enemyClock = timer:new(5)

end

function PlayState:update(dt)

	gameClock:updateTime(dt)

	willSpawn = enemyClock:updateTime(dt)

	if willSpawn then

		local enemy = Enemy:new()

		enemy.x = math.random(love.graphics.getWidth())
		enemy.y = math.random(love.graphics.getHeight())

		table.insert(world.enemies, enemy)

	end

	for w, u in ipairs(player.bullets) do

		u:update(dt)

		for x, v in ipairs(world.enemies) do

			local collide =	Entity.checkCollision(u, v)

			if collide and Entity.sameColor(player, v) then

				table.remove(world.enemies, x)
				table.remove(player.bullets, w)

			end

		end

		if Entity.checkOutOfBounds(u) then table.remove(player.bullets, w) end

	end

	for i, v in ipairs(world.enemies) do

		v:update(dt)

		for i2, v2 in ipairs(v.bullets) do

			local collide = Entity.checkCollision(v2, player)

			if collide then

				player.color.r = v.color.r
				player.color.g = v.color.g
				player.color.b = v.color.b
				player.heart = player.heart - 1

				table.remove(v.bullets, i2)

			end

		end

	end

	player:update(dt)

end

function PlayState:draw()

	State.draw(self)

	player:draw()

	for i, v in ipairs(player.bullets) do
		v:draw()
	end

	for i, v in ipairs(world.enemies) do
		v:draw()
		for w, m in ipairs(v.bullets) do
			m:draw()
		end
		love.graphics.setColor(255, 255, 255, 255)
	end

	love.graphics.setColor(255, 255, 255, 255)
	
	for i = 1, player.heart, 1 do
		love.graphics.draw(heartImage, 75 * i, 10)
	end

end