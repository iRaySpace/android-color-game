Player = Entity:subclass('Player')

function Player:initialize(x, y)

	local width = 32
	local height = 32
	local speed = 250

	Entity.initialize(self, x, y, width, height, speed)

	self.color = {}
	self.color.r = 255
	self.color.g = 255
	self.color.b = 255

	self.heart = 3

	self.bullets = {}

end

function Player:update(dt)

	Entity.update(self, dt)

	self.velocity.x = 0
	self.velocity.y = 0

	if love.keyboard.isDown("up") then self.velocity.y = -self.speed
	elseif love.keyboard.isDown("down") then self.velocity.y = self.speed
	elseif love.keyboard.isDown("left") then self.velocity.x = -self.speed
	elseif love.keyboard.isDown("right") then self.velocity.x = self.speed
	end

end

function Player:draw()

	love.graphics.setColor(self.color.r, self.color.g, self.color.b, 255)

	Entity.draw(self)

end