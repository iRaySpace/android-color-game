Android Color Game 
-------------------
This is a repository of our game development project.

UPDATE 05/30/2014
I wanted it to be available in public and probably make it a community project. If you are interested to contribute, please tell me.


version logs
-------------

version 0.1 :

- added bullet features
- added timer features
- added enemy features
- added player features

version 0.2 :

- added entity as the base
- added middleclass for object orientation simulation (see kikito@github)

- refactor a lot of codes within main to other files

- class will be deleted 

version 0.2a :

- refactoring a lot of codes
- added new functions on entity
- added new functions on bullet

version 0.2b :

- making states for the game
- state as the base
- refactor most of the game rendering to playstate
- added font '8-BIT WONDER' from dafonts

version 0.2c :

- added title image
- added gameover states
- updating playstates and menustates
- added health system
- added colors

version 0.2d :

- added no collision over not the same color feature
- added ai prevent bumping towards the edge

BUGS

- when at the edge don't move towards any other sides 

future version
---------------

version 0.3 :

- working main menu
- working game with healths and powerups

version 0.4 :

- polishing game art
- add new features

version 0.5 :

- porting to android