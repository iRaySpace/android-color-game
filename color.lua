local class = require "middleclass"

Color = class('Color')

local red = {}
red.r = 239
red.g = 27
red.b = 85

local green = {}
green.r = 134
green.g = 249
green.b = 180

local blue = {}
blue.r = 19
blue.g = 187
blue.b = 236

function Color:initialize()
    
    local random = love.math.random(2)
    
    if random == 1 then
    	self.r, self.g, self.b = red.r, red.g, red.b
    elseif random == 2 then
    	self.r, self.g, self.b = green.r, green.g, green.b
    else
    	self.r, self.g, self.b = blue.r, blue.g, blue.b
    end
   
end