local class = require "middleclass"

State = class("State")

function State:initialize(name)

	self.name = name
	self.objects = {}

end

function State:update(dt) end
function State:draw() end