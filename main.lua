function love.load()

	world = {}
	world.enemies = {}

	require "class"
	require "color"
	require "entity"
	require "bullet"
	require "enemy"
	require "timer"
	require "player"
	require "fsm"
	require "state"
	require "playstate"
	require "menustate"
	require "gameoverstate"
	
	gameState = MenuState:new()

	font = love.graphics.newFont("8-BIT WONDER.TTF", 27)

	math.randomseed(os.time())
	math.random(); math.random(); math.random()

	love.graphics.setFont(font)

	heartImage = love.graphics.newImage("heart.png")

end

function love.update(dt)

	gameState:update(dt)

	if gameState.name == "Menu" then
		if love.mouse.isDown("l") then
			gameState = PlayState:new()
		end
	elseif gameState.name == "Play" then
		if player.heart == 0 then
			gameState = GameOverState:new()
		end
	elseif gameState.name == "GameOver" then
		if love.mouse.isDown("l") then
			gameState = MenuState:new()
		end
	end

end

function love.draw()

	gameState:draw()

end

function love.keypressed(key)

	local bullet = Bullet:new()

	if key == "w" then

		bullet:setPosition(player.x + Entity.getHalfWidth(player), player.y)
		bullet:setVelocity(0, -bullet.speed)

		table.insert(player.bullets, bullet)

	elseif key == "s" then

		bullet:setPosition(player.x + Entity.getHalfWidth(player), player.y + player.height)
		bullet:setVelocity(0, bullet.speed)

		table.insert(player.bullets, bullet)

	elseif key == "a" then

		bullet:setPosition(player.x, player.y + Entity.getHalfHeight(player))
		bullet:setVelocity(-bullet.speed, 0)

		table.insert(player.bullets, bullet)

	elseif key == "d" then

		bullet:setPosition(player.x + player.width, player.y + Entity.getHalfHeight(player))
		bullet:setVelocity(bullet.speed, 0)

		table.insert(player.bullets, bullet)

	end

end