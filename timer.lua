timer = class:new()

function timer:init(max)

	self.max = max or 0
	self.currentTime = 0

end

function timer:resetTime() self.currentTime = 0 end
function timer:updateTime(dt)

	if self.max then
		if self.currentTime > self.max then self:resetTime() return true end
	end

	self.currentTime = self.currentTime + dt

	return false

end