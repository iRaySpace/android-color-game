MenuState = State:subclass("MenuState")

function MenuState:initialize()

	local name = "Menu"

	State.initialize(self, name)

	self.titleImage = love.graphics.newImage("title.png")

end

function MenuState:update(dt)

end

function MenuState:draw()

	State.draw(self)

	love.graphics.draw(self.titleImage, self.titleImage:getWidth() / 2, 100)

	love.graphics.printf("tap the screen to play", love.window.getWidth() / 2 - 275, love.window.getHeight() / 2, 550, "center")

end