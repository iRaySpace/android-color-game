Bullet = Entity:subclass('Bullet')

function Bullet:initialize(x, y)

	local width = 8
	local height = 8
	local speed = 500

	Entity.initialize(self, x, y, width, height, speed)

end

function Bullet:update(dt)

	Entity.update(self, dt)

end

function Bullet:setPosition(x, y)

	self.x = x
	self.y = y

end

function Bullet:setVelocity(x, y)

	self.velocity.x = x
	self.velocity.y = y

end