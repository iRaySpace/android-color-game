GameOverState = State:subclass("GameOverState")

function GameOverState:initialize()

	local name = "GameOver"

	State.initialize(self, name)

	self.titleImage = love.graphics.newImage("title.png")

end

function GameOverState:update(dt)
    
end

function GameOverState:draw()

	State.draw(self)

	love.graphics.draw(self.titleImage, self.titleImage:getWidth() / 2, 100)

	love.graphics.printf("game overrrrrrr!!", love.window.getWidth() / 2 - 275, love.window.getHeight() / 2, 550, "center")

end