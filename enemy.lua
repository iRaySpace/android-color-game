Enemy = Entity:subclass('Enemy')

function Enemy:initialize(x, y, width, height, speed)

	Entity.initialize(self, x, y, width, height, speed)

	self.move = {}
	self.move.x = 0
	self.move.y = 0

	self.delay = timer:new(2)
	self.delayFire = timer:new(1)

	self.bullets = {}

	self.color = {}
	self.color = Color:new()

end

function Enemy:moveDistance(distance, dir)
	
	self.velocity.x = 0
	self.velocity.y = 0

	self.move.x = 0
	self.move.y = 0

	if dir == 1 then
		self.velocity.y = -self.speed
		self.move.y = distance
	elseif dir == 2 then
		self.velocity.y = self.speed
		self.move.y = distance
	elseif dir == 3 then
		self.velocity.x = -self.speed
		self.move.x = distance
	else
		self.velocity.x = self.speed
		self.move.x = distance
	end

end

function Enemy:update(dt)

	local willMove = self.delay:updateTime(dt)
	local willFire = self.delayFire:updateTime(dt)

	Entity.update(self, dt)

	self:checkBounds()
	self:updateBullet(dt)

	if willMove then self:moveDistance(math.random(100, 200), math.random(4)) end
	if willFire then self:fireBullet(math.random(4)) end

	if Entity.isNearEdge(self) then
	    self.velocity.x = -self.velocity.x
	    self.velocity.y = -self.velocity.y
	end

end

function Enemy:draw()

	love.graphics.setColor(self.color.r, self.color.g, self.color.b, 255)

	Entity.draw(self)

end

function Enemy:updateBullet(dt)

	for i, v in ipairs(self.bullets) do

		v:update(dt)

		if Entity.checkOutOfBounds(v) then table.remove(self.bullets, i) end

	end

end

function Enemy:checkBounds()

	if self.x < 0 then self.x = 0 end
	if self.y < 0 then self.y = 0 end
	if self.x + self.width > love.window.getWidth() then self.x = love.window.getWidth() - self.width end
	if self.y + self.height > love.window.getHeight() then self.y = love.window.getHeight() - self.height end	

end

function Enemy:fireBullet(dir)

	local bullet = Bullet:new()

	if dir == 1 then

		bullet:setPosition(self.x + self.width / 2, self.y)
		bullet:setVelocity(0, -bullet.speed)

		table.insert(self.bullets, bullet)

	elseif dir == 2 then

		bullet:setPosition(self.x + self.width / 2, self.y + self.height)
		bullet:setVelocity(0, bullet.speed)
		
		table.insert(self.bullets, bullet)

	elseif dir == 3 then

		bullet:setPosition(self.x, self.y + self.height / 2)
		bullet:setVelocity(-bullet.speed, 0)

		table.insert(self.bullets, bullet)

	else 

		bullet:setPosition(self.x + self.width, self.y + self.height / 2)
		bullet:setVelocity(bullet.speed, 0)

		table.insert(self.bullets, bullet)

	end

end