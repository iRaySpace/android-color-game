local class = require "middleclass"

Entity = class("Entity")

function Entity:initialize(x, y, width, height, speed)

	self.x = x or 0
	self.y = y or 0

	self.width = width or 16
	self.height = height or 16

	self.velocity = {}
	self.velocity.x = 0
	self.velocity.y = 0

	self.speed = speed or 250

end

function Entity:update(dt)

	self.x = self.x + self.velocity.x * dt
	self.y = self.y + self.velocity.y * dt

end

function Entity:draw()

	love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)

end

function Entity.static:getHalfWidth() return self.width / 2 end
function Entity.static:getHalfHeight() return self.height / 2 end

function Entity.static:checkOutOfBounds()

	if self.x < 0 or self.x > love.window.getWidth() then return true end
	if self.y < 0 or self.y > love.window.getHeight() then return true end

end

function Entity.static:checkCollision(other)

	if self.x < other.x + other.width and self.x + self.width > other.x and self.y < other.y + other.height and self.y + self.height > other.y then return true end
	return false

end

function Entity.static:sameColor(other)

	if self.color.r == other.color.r and self.color.g == other.color.g and self.color.b == other.color.b then return true end
	return false

end

function Entity.static:isNearEdge()
    
    if self.x + (self.width * 2) > love.window.getWidth() then return true
    elseif self.x - self.width < 0 then return true
    elseif self.y + (self.height * 2) > love.window.getHeight() then return true
    elseif self.y - self.height < 0 then return true end
    
    return false
    
end